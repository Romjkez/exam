import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PurchasesDatabaseService } from '../../services/purchases-database.service';
import { Purchase } from '../../models/purchase';

@Component({
  selector: 'app-edit-purchase',
  templateUrl: './edit-purchase.component.html',
  styleUrls: ['./edit-purchase.component.scss']
})
export class EditPurchaseComponent implements OnInit {

  purchase: Purchase;

  form: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.minLength(2)]),
    quantity: new FormControl('', [Validators.required, Validators.min(1)])
  });

  constructor(private activatedRoute: ActivatedRoute,
              private purchasesDb: PurchasesDatabaseService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(async data => {
      this.purchase = await this.purchasesDb.getOneById(data.id);

      this.form.setValue({title: this.purchase.title, quantity: this.purchase.quantity});
    });
  }

  async onDelete(): Promise<void> {
    await this.purchasesDb.deleteOneById(this.purchase.id)
      .then(() => this.router.navigateByUrl('/purchases'));
  }

  async onSubmit(): Promise<void> {
    await this.purchasesDb.putOneById(this.purchase.id, {
      title: this.form.get('title').value,
      quantity: this.form.get('quantity').value,
      isPurchased: this.purchase.isPurchased,
    })
      .then(() => this.router.navigateByUrl('/purchases'));
  }

}
