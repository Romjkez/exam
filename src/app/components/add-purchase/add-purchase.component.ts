import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PurchasesDatabaseService } from '../../services/purchases-database.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-purchase',
  templateUrl: './add-purchase.component.html',
  styleUrls: ['./add-purchase.component.scss']
})
export class AddPurchaseComponent {

  form: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.minLength(2)]),
    quantity: new FormControl('', [Validators.required, Validators.min(1)])
  });

  constructor(private purchasesDb: PurchasesDatabaseService,
              private router: Router) {
  }

  async onSubmit(): Promise<void> {
    const data = Object.assign(this.form.value, {isPurchased: false});
    await this.purchasesDb.postOne(data)
      .then(() => this.router.navigateByUrl('/purchases'));
  }
}
