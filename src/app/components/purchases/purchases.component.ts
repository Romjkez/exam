import { Component, OnInit } from '@angular/core';
import { PurchasesDatabaseService } from '../../services/purchases-database.service';
import { Purchase } from '../../models/purchase';
import { SortType } from '../../pipes/sortByTitle.pipe';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.scss']
})
export class PurchasesComponent implements OnInit {
  sortType: SortType = SortType.ASC;
  purchases: Promise<Purchase[]>;

  constructor(private purchasesDb: PurchasesDatabaseService) {
  }

  ngOnInit(): void {
    this.purchases = this.getPurchases();
  }

  toggleSortType(): void {
    if (this.sortType === SortType.ASC) {
      this.sortType = SortType.DESC;
    } else {
      this.sortType = SortType.ASC;
    }
  }

  async getPurchases(): Promise<Purchase[]> {
    return this.purchasesDb.getAll();
  }

  async toggleStatus(purchase: Purchase): Promise<void> {
    await this.purchasesDb.putOneById(purchase.id, Object.assign(purchase, {isPurchased: !purchase.isPurchased}));
    this.purchases = this.getPurchases();
  }
}
